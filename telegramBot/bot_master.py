import sys
import os
sys.path.insert(1, os.getcwd())

import hashlib
import logging.config
import os
import operator
import cherrypy
import psycopg2
import telebot
import yaml
import yaml_master
from telegramBot import DB
from OSfuncs import wireguard
from OSfuncs import ssh_logs
from OSfuncs import fail2ban
from OSfuncs import rkhunter
from OSfuncs import exportpdf


class Main(object):
    salt_len = 32
    roles = {
        'selectMe': [1, 2, 3, 4, 5, 6, 7, 8],
        'select': [2, 4, 5, 6, 7, 8],
        'updateMe': [3, 4, 6, 7, 8],
        'update': [4, 6, 7, 8],
        'insert': [5, 6, 8],
        'delete': [7, 8]
    }
    with open('settings.yaml') as f:
        config = yaml.safe_load(f)
    user_ids_name_group = {}
    bot = telebot.TeleBot(config['token'], threaded=False)
    bd = DB.DataBase()
    users_login_pass_group = bd.user_pass()
    logins = [step[0] for step in users_login_pass_group]
    logger = logging.getLogger('main_logger')
    logging_config = config['logging_config']
    logging.config.dictConfig(logging_config)

    def message_handlers(self):
        @self.bot.message_handler(func=lambda x: self.is_logged_in(x) is False, commands=["login"])
        def login(message):
            try:
                data = message.text.split()[1:]
                if len(data) == 2:
                    self.users_update()
                    if data[0] in self.logins and \
                            self.right_pass(data[1],
                                            self.users_login_pass_group[self.logins.index(data[0])][1]):
                        if len(self.user_ids_name_group) == 0 or data[0] not in \
                                list(zip(*self.user_ids_name_group.values()))[0]:
                            self.user_ids_name_group[message.from_user.id] = \
                                [self.users_login_pass_group[self.logins.index(data[0])][0],
                                 self.users_login_pass_group[self.logins.index(data[0])][2]]
                            self.bot.send_message(message.chat.id,
                                                  "Вход в аккаунт выполнен успешно")
                            self.logger.info('User %s has successfully logged in', (data[0],))
                        else:
                            self.bot.send_message(message.chat.id,
                                                  "Такой пользователь уже зашёл в аккаунт")
                            self.logger.warning('User %s has unsuccessfully tried to log in', (data[0],))
                    else:
                        self.bot.send_message(message.chat.id,
                                              "Такого пользователя не существует")
                        self.logger.warning('User %s has unsuccessfully tried to log in', (data[0],))

                else:
                    self.bot.send_message(message.chat.id,
                                          "Неверные данные. Нужный формат: \n"
                                          "/login username password")
            except:
                self.bot.send_message(message.chat.id,
                                      "Где-то произошла ошибка. Проверьте данные "
                                      "и попробуйте ещё раз")
                self.logger.exception("Exception occurred while trying to login")
                raise

        @self.bot.message_handler(func=lambda x: self.is_logged_in(x), commands=["logout"])
        def logout(message):
            try:
                data = message.text.split()[1:]
                if len(data) == 0:
                    if message.from_user.id in self.user_ids_name_group:
                        del self.user_ids_name_group[message.from_user.id]
                        self.bot.send_message(message.chat.id,
                                              "Выход из аккаунта был выполнен успешно")
                        self.logger.info('User with id %s has successfully logged out', (message.from_user.id,))
                    else:
                        self.bot.send_message(message.chat.id,
                                              "Вы не зашли в аккаунт")
                else:
                    self.bot.send_message(message.chat.id,
                                          "Неверные данные. Нужный формат: \n"
                                          "/logout")
            except:
                self.bot.send_message(message.chat.id,
                                      "Где-то произошла ошибка. Проверьте данные "
                                      "и попробуйте ещё раз")
                self.logger.exception("Exception occurred while trying to logout")
                raise

        @self.bot.message_handler(func=lambda x: self.is_logged_in(x) and self.is_allowed(self.get_role(x), 'insert'),
                                  commands=["register"])
        def register(message):
            data = message.text.split()[1:]
            keepalive = 25
            ip_address = ""
            send_ip = True
            if len(data) >= 3 and data[2].isdigit() and self.strong_pass(data[1]):
                try:
                    if data[0] not in self.logins:
                        self.bd.insert_user(data[0], self.pass_gen(data[1]), data[2])
                        self.users_update()
                        for find_ip in data[3:]:
                            if self.is_ip(find_ip):
                                ip_address = find_ip
                                send_ip = False
                                break
                        for find_int in data[3:]:
                            if find_int.isdigit():
                                keepalive = int(find_int)
                                break
                        else:
                            self.bot.send_message(message.chat.id,
                                                  "keep_alive не найден. Сохранено с {}".format(keepalive))

                        path, ip_address = self.add_wireguard_user(data[0], ip_address, keepalive)
                        if send_ip:
                            self.bot.send_message(message.chat.id,
                                                  "ip_address не найден. Сохранено с {}".format(ip_address))
                        config = open(path, "rb")
                        self.bot.send_document(message.chat.id, config)
                        self.bot.send_message(message.chat.id, "Регистрация пользователя с логином {0} "
                                                               "произошла успешно".format(data[0]))
                        self.logger.info('User {} {} has successfully registered'.format(data[0], data[1]))
                    else:
                        self.bot.send_message(message.chat.id, "Пользователь с таким именем уже существует")

                except:  # Application must go on any way regardless of errors
                    self.bot.send_message(message.chat.id, "Где-то произошла ошибка. Проверьте данные "
                                                           "и попробуйте ещё раз")
                    self.logger.exception('Exception occurred while trying to register %s', (data[0],))
            else:
                self.bot.send_message(message.chat.id, "Неверные данные. Нужный формат: "
                                                       "\n/register username password role(digit) ip_address=rand_ip "
                                                       "keep_alive=25 "
                                                       "\nОбратите внимание, что в пароле должно быть не менее 8 "
                                                       "символов, "
                                                       "пароль должен содержать строчные и прописные символы "
                                                       "английского алфавита, "
                                                       "а также содержать специальные символы из списка: '!@#$%^&*(){"
                                                       "}[];:'|/?.,<>'")

        @self.bot.message_handler(func=lambda x: self.is_logged_in(x) and self.is_allowed(self.get_role(x), 'update'),
                                  commands=["edit_user"])
        def edit_user(message):
            data = message.text.split()[1:]
            if len(data) == 2:
                try:
                    if data[0] in self.logins and data[1].isdigit():
                        if data[0] != self.get_login(message) and data[0] in self.lower_usernames(
                                self.get_role(message)) \
                                and int(data[1]) < self.get_role(message):
                            self.bd.update_user(data[0], int(data[1]))
                            self.users_update()
                            self.bot.send_message(message.chat.id,
                                                  "Изменение пользователя {} произошло успешно".format(data[0]))
                            self.logger.info('User %s has successfully edited', (data[0],))
                        else:
                            self.bot.send_message(message.chat.id, "Нельзя изменить себя и пользователей выше статусом")
                    else:
                        self.bot.send_message(message.chat.id, "Пользователя с таким логином не существует")
                except:  # Application must go on any way regardless of errors
                    self.bot.send_message(message.chat.id, "Где-то произошла ошибка. Проверьте данные "
                                                           "и попробуйте ещё раз")
                    self.logger.exception('Exception occurred while trying to edit user %s', (data[0],))
            else:
                self.bot.send_message(message.chat.id, "Неверные данные. Нужный формат: "
                                                       "\n/editUser username role")

        @self.bot.message_handler(func=lambda x: self.is_logged_in(x) and self.is_allowed(self.get_role(x), 'delete'),
                                  commands=["delete_user"])
        def del_user(message):
            data = message.text.split()[1:]
            if len(data) == 1:
                try:
                    if data[0] in self.logins:
                        if data[0] != self.get_login(message):

                            self.bd.del_user(data[0])
                            self.del_wireguard_user(data[0])
                            self.users_update()
                            self.bot.send_message(message.chat.id,
                                                  "Удаление пользователя {} произошло успешно".format(data[0]))
                            self.logger.info('User %s has successfully deleted', (data[0],))
                        else:
                            self.bot.send_message(message.chat.id, "Нельзя удалить себя")
                    else:
                        self.bot.send_message(message.chat.id, "Пользователя с таким логином не существует")
                except:  # Application must go on any way regardless of errors
                    self.bot.send_message(message.chat.id, "Где-то произошла ошибка. Проверьте данные "
                                                           "и попробуйте ещё раз")
                    self.logger.exception('Exception occurred while trying to delete user %s', (data[0],))
            else:
                self.bot.send_message(message.chat.id, "Неверные данные. Нужный формат: "
                                                       "\n/deleteUser username")

        @self.bot.message_handler(func=lambda x: self.is_logged_in(x) and self.is_allowed(self.get_role(x), 'delete'),
                                  commands=["clear_logged"])
        def clear_logged_users(message):
            self.bot.send_message(message.chat.id, "Все пользователи принудительно вышли из аккаунтов")
            self.logger.info('All logged in users have been cleared')
            self.user_ids_name_group = {}

        @self.bot.message_handler(func=lambda x: self.is_logged_in(x) and self.is_allowed(self.get_role(x), 'select'),
                                  commands=["export_as_pdf"])
        def export_as_pdf(message):
            try:
                exportpdf.ExportPdf(self.bd)
                if os.path.exists('/var/log/report.pdf'):
                    self.bot.send_document(message.chat.id, open('/var/log/report.pdf', 'rb'))
                else:
                    self.bot.send_message(message.chat.id, "Где-то произошла ошибка. Проверьте данные "
                                                           "и попробуйте ещё раз")
            except:  # Application must go on any way regardless of errors
                self.logger.exception("Exception occurred while trying to show ssh")
                self.bot.send_message(message.chat.id, "Где-то произошла ошибка. Проверьте данные "
                                                       "и попробуйте ещё раз")

        @self.bot.message_handler(func=lambda x: self.is_logged_in(x) and self.is_allowed(self.get_role(x), 'select'),
                                  commands=["show_ssh"])
        def show_ssh(message):
            try:
                filtered_data_fail, filtered_data_accepted = ssh_logs.parse_log()
                if filtered_data_accepted:
                    self.bot.send_message(message.chat.id, "Принятые данные:\n"+"\n".join(filtered_data_accepted[-20::]))
                else:
                    self.bot.send_message(message.chat.id,
                                          "Принятые данные:\nОтсутствуют")
                if filtered_data_fail:
                    self.bot.send_message(message.chat.id,
                                          "Некорректные данные:\n" + str(filtered_data_fail[-20::]))
                else:
                    self.bot.send_message(message.chat.id,
                                          "Некорректные данные:\nОтсутствуют")
            except:  # Application must go on any way regardless of errors
                self.logger.exception("Exception occurred while trying to show ssh")
                self.bot.send_message(message.chat.id, "Где-то произошла ошибка. Проверьте данные "
                                                       "и попробуйте ещё раз")

        @self.bot.message_handler(
            func=lambda x: self.is_logged_in(x) and self.is_allowed(self.get_role(x), 'update'),
            commands=["show_rkhunter_log"])
        def show_rkhunter_log(message):
            try:
                filtered_data = rkhunter.parse_log()
                if filtered_data:
                    self.bot.send_message(message.chat.id, "Логи rkhunter:\n" + "\n".join(filtered_data[-20::]))
                else:
                    self.bot.send_message(message.chat.id,
                                          "Логи rkhunter:\nОтсутствуют")
            except:  # Application must go on any way regardless of errors
                self.logger.exception("Exception occurred while trying to show rkhunter logs")
                self.bot.send_message(message.chat.id, "Где-то произошла ошибка. Проверьте данные "
                                                       "и попробуйте ещё раз")

        @self.bot.message_handler(
            func=lambda x: self.is_logged_in(x) and self.is_allowed(self.get_role(x), 'select'),
            commands=["find_ip_fail2ban"])
        def find_ip_fail2ban(message):
            data = message.text.split()[1:]
            if len(data) == 2:
                try:
                    find_by, ip_add = data
                    result = fail2ban.find_in_db(find_by, ip_add)
                    if result:
                        self.bot.send_message(message.chat.id, "Итоговые данные:\n" + "\n".join(result))
                    else:
                        self.bot.send_message(message.chat.id, "Итоговые данные:\nОтсутствуют")
                except:  # Application must go on any way regardless of errors
                    self.logger.exception("Exception occurred while trying to find fail2ban")
                    self.bot.send_message(message.chat.id, "Где-то произошла ошибка. Проверьте данные "
                                                           "и попробуйте ещё раз")
            else:
                self.bot.send_message(message.chat.id, "Неверные данные. Нужный формат: "
                                                       "\n/find_ip_fail2ban find_by ip_add")

        @self.bot.message_handler(func=lambda x: self.is_logged_in(x) and self.is_allowed(self.get_role(x), 'select'),
                                  commands=["show_users"])
        def show_users(message):
            try:
                self.users_update()
                if self.logins:
                    self.bot.send_message(message.chat.id, "name, group\n" + "\n".join(
                        [step[0] + ", " + str(step[2]) for step in self.users_login_pass_group]))
                    self.logger.info("Возможные пользователи: \n" + "\n".join(["{} : {}".format(group[0], group[2])
                                                                               for group in
                                                                               self.users_login_pass_group]))
                else:
                    self.bot.send_message(message.chat.id, "Пользователей нет")
            except:  # Application must go on any way regardless of errors
                self.logger.exception("Exception occurred while trying to show users")
                self.bot.send_message(message.chat.id, "Где-то произошла ошибка. Проверьте данные "
                                                       "и попробуйте ещё раз")

        @self.bot.message_handler(func=lambda x: self.is_logged_in(x) and self.is_allowed(self.get_role(x), 'select'),
                                  commands=["show_system_users"])
        def show_system_users(message):
            try:
                sys_users, headers = self.bd.select_system_users()[0:2]
                sys_users = sorted(sys_users, key=lambda k: k[-1], reverse=True)
                if len(sys_users) > 0:
                    to_send = ", ".join(headers[1:]) + "\n" + "\n".join(
                        [", ".join(map(str, step[1:])) for step in sys_users])
                    self.bot.send_message(message.chat.id, to_send)
                    self.logger.info("Системные пользователи: \n" + to_send)
                else:
                    self.bot.send_message(message.chat.id, "Системных пользователей нет")
            except:  # Application must go on any way regardless of errors
                self.logger.exception("Exception occurred while trying to show system users")
                self.bot.send_message(message.chat.id, "Где-то произошла ошибка. Проверьте данные "
                                                       "и попробуйте ещё раз")

        @self.bot.message_handler(func=lambda x: self.is_logged_in(x) and self.is_allowed(self.get_role(x), 'select'),
                                  commands=["show_wireguard_users"])
        def show_wireguard_users(message):
            try:
                wireguard_users, headers = self.bd.select_wg_users()[0:2]
                if len(wireguard_users) > 0:
                    to_send = ", ".join(headers[1:2] + headers[4:]) + "\n" + "\n".join(
                        [", ".join(map(str, step[1:2] + step[4:])) for step in wireguard_users])
                    self.bot.send_message(message.chat.id, to_send)
                    self.logger.info("Пользователи wireguard: \n" + to_send)
                else:
                    self.bot.send_message(message.chat.id, "Пользователей wireguard нет")
            except:  # Application must go on any way regardless of errors
                self.logger.exception("Exception occurred while trying to show wireguard users")
                self.bot.send_message(message.chat.id, "Где-то произошла ошибка. Проверьте данные "
                                                       "и попробуйте ещё раз")

        @self.bot.message_handler(func=lambda x: self.is_logged_in(x) and self.is_allowed(self.get_role(x), 'select'),
                                  commands=["show_logged_users"])
        def show_logged_users(message):
            try:
                if self.user_ids_name_group:
                    self.bot.send_message(message.chat.id,
                                          "name, user_tg_id, group\n" + "\n".join([list(stop)[1][0] + ", " +
                                                                                   str(list(stop)[0]) + ", " + str(
                                              list(stop)
                                              [1][1]) for stop in self.user_ids_name_group.items()]))
                    self.logger.info("Зарегестрированные пользователи: \n" + "\n".join(
                        ["{} : {} : {}".format(self.user_ids_name_group[key][0], key,
                                               self.user_ids_name_group[key][1]) for key in
                         self.user_ids_name_group.keys()]))
                else:
                    self.bot.send_message(message.chat.id, "Авторизированных пользователей нет")
            except:  # Application must go on any way regardless of errors
                self.logger.exception("Exception occurred while trying to show users")
                self.bot.send_message(message.chat.id, "Где-то произошла ошибка. Проверьте данные "
                                                       "и попробуйте ещё раз")

        @self.bot.message_handler(func=lambda x: self.is_logged_in(x) and self.is_allowed(self.get_role(x), 'selectMe'),
                                  commands=["show_my_info"])
        def show_my_info(message):
            try:
                self.users_update()
                if self.logins:
                    self.bot.send_message(message.chat.id, "name, group\n {}, {}".format(
                        *self.user_ids_name_group[message.from_user.id]))
                    self.logger.info("User {} watched for his info".format(self.get_login(message)))
                else:
                    self.bot.send_message(message.chat.id, "Пользователей нет")
            except:  # Application must go on any way regardless of errors
                self.logger.exception("Exception occurred while trying to show your info")
                self.bot.send_message(message.chat.id, "Где-то произошла ошибка. Проверьте данные "
                                                       "и попробуйте ещё раз")

        @self.bot.message_handler(
            func=lambda x: self.is_logged_in(x) and self.is_allowed(self.get_role(x), 'delete') and self.is_allowed(
                self.get_role(x), 'insert'), commands=["reconnect"])
        def reconnect(message):

            data = message.text.split()[1:]
            if len(data) in [0, 5]:
                try:
                    self.bd.stop()
                    self.bd.start(*data)
                    if len(data) == 5:
                        yml = yaml_master.YamlMaster()
                        temp_arr = ['base_host', 'base_port', 'base_dbname', 'base_user', 'base_password']
                        yml.change({temp_arr[step]: data[step] for step in range(5)})
                    self.bot.send_message(message.chat.id, "Подключение к бд прошло успешно")
                    self.logger.info('New connection has been added')
                except psycopg2.Error:
                    self.bot.send_message(message.chat.id, "Не существует бд с такими параметрами")
                    self.logger.warning('Connection has been unsuccessfully tried to change')
                    try:
                        self.bd.start()
                        self.user_ids_name_group = {}
                        self.bot.send_message(message.chat.id,
                                              "Удалось подключиться к бд со стандартными параметрами")
                        self.logger.warning('Connection has been recovered')
                    except psycopg2.Error:
                        self.bot.send_message(message.chat.id,
                                              "Не удалось подключиться к бд со стандартными параметрами")
                        self.logger.warning('Connection has been lost')

            else:
                self.bot.send_message(message.chat.id, "Неверное количество параметров\n"
                                                       "/reconnect host, port, dbname, user, password\n"
                                                       "/reconnect для подключения с базовыми параметрами")
                self.logger.warning('Connection has been unsuccessfully tried to change')

        @self.bot.message_handler(func=lambda x: self.is_logged_in(x) and self.is_allowed(self.get_role(x), 'update'),
                                  commands=["edit_log_level"])
        def edit_log_level(message):
            data = message.text.split()[1:]
            levels = ["DEBUG", "INFO", "WARNING", "ERROR", "CRITICAL"]
            if len(data) == 1 and data[0].upper() in levels:
                try:
                    level = data[0].upper()
                    yml = yaml_master.YamlMaster()
                    yml.change({'level': level})
                    self.logger.setLevel(eval("logging.{}".format(level)))
                    self.bd.logger.setLevel(eval("logging.{}".format(level)))
                    self.bot.send_message(message.chat.id, f"Уровень для сообщений лога {level.upper()} "
                                                           "установлен. Для полного обновления функционала нужен "
                                                           "перезапуск")
                except(ValueError, SyntaxError):
                    self.bot.send_message(message.chat.id, "Где-то произошла ошибка. Проверьте данные "
                                                           "и попробуйте ещё раз")
                    self.logger.exception('Exception occurred during logging level changing')
            else:
                self.bot.send_message(message.chat.id, "Нет такого уровня.")
                self.logger.warning('logging level has been unsuccessfully tried to change')

        @self.bot.message_handler(func=lambda x: self.is_logged_in(x) and self.is_allowed(self.get_role(x), 'update'),
                                  commands=["edit_log_chat"])
        def edit_log_chat(message):
            data = message.text.split()[1:]
            if len(data) == 1 and data[0].isdigit():
                try:
                    chat = int(data[0])
                    yml = yaml_master.YamlMaster()
                    yml.change({'chat': chat})
                    self.bot.send_message(message.chat.id, f"Чат для сообщений лога {chat} "
                                                           "установлен. Для полного обновления функционала нужен "
                                                           "перезапуск")
                except(ValueError, SyntaxError):
                    self.bot.send_message(message.chat.id, "Где-то произошла ошибка. Проверьте данные "
                                                           "и попробуйте ещё раз")
                    self.logger.exception('Exception occurred during logging chat changing')
            else:
                self.bot.send_message(message.chat.id, "Нужно число, например -1001492565750. Не забудьте добавить бота"
                                                       "в чат")
                self.logger.warning('logging chat has been unsuccessfully tried to change')

        @self.bot.message_handler(content_types=["text"])
        @self.bot.message_handler(commands=["start"])
        def start(message):
            if not self.is_logged_in(message):
                self.bot.send_message(message.chat.id,
                                      "Привет. Войди в аккаунт для продолжения.\n"
                                      "/login login password")
            else:
                roles = self.bd.get_permissions()
                self.bot.send_message(message.chat.id,
                                      "Ваши полномочия:\n" + roles[self.get_role(message) - 1][0] +
                                      "\n\nВозможные для выполнения команды:\n" + "\n".join(["\t".join(step)
                                                                                             for step in
                                                                                             self.get_bot_commands(
                                                                                                 self.get_role(
                                                                                                     message))]))

    def pass_gen(self, password):
        salt = os.urandom(self.salt_len)
        key = hashlib.pbkdf2_hmac('sha512', password.encode('utf-16'), salt, 100000)
        return salt + key

    def right_pass(self, password, hashedpass):
        try:
            right = hashlib.pbkdf2_hmac('sha512', password.encode('utf-16'), hashedpass[:self.salt_len],
                                        100000) == hashedpass[self.salt_len:]
            return right
        except ValueError:
            self.logger.exception("Exception occurred while trying to compare passwords")
            raise

    def lower_usernames(self, role=0):
        return [step[0] for step in self.users_login_pass_group if step[2] < role]

    @staticmethod
    def strong_pass(password):
        upper_set = set('ABCDEFGHIJKLMNOPQRSTUVWXYZ')
        lower_set = set('abcdefghijklmnopqrstuvwxyz')
        digit_set = set('1234567890')
        spec_set = set("!@#$%^&*(){}[];:'|/?.,<> ")
        if len(password) < 8:
            return False
        password_set = set(password)
        return (password_set & upper_set and
                password_set & lower_set and
                password_set & digit_set and
                password_set & spec_set)

    def is_logged_in(self, message):
        if message.from_user.id in self.user_ids_name_group:
            return True
        return False

    def get_login(self, message):
        return self.user_ids_name_group[message.from_user.id][0]

    def get_role(self, message):
        if hasattr(message, 'from_user'):
            return self.user_ids_name_group[message.from_user.id][1]
        else:
            group = [step[2] for step in self.users_login_pass_group if step[0] == message]
            if len(group) > 0:
                return group[0]
            return 0

    def is_allowed(self, role, permission):
        return role in self.roles[permission]

    def users_update(self):
        self.users_login_pass_group = self.bd.user_pass()
        self.logins = [self.users_login_pass_group[step][0] for step in range(len(self.users_login_pass_group))]
        to_del = []
        for item in self.user_ids_name_group.items():
            if item[1][0] in self.logins:
                self.user_ids_name_group[item[0]][1] = self.get_role(item[1][0])
            else:
                to_del.append(item[0])
        for item in to_del:
            self.user_ids_name_group.pop(item)
        self.logger.info('Users info have been updated from database')

    @cherrypy.expose
    def index(self):
        if 'content-length' in cherrypy.request.headers and \
                'content-type' in cherrypy.request.headers and \
                cherrypy.request.headers['content-type'] == 'application/json':
            length = int(cherrypy.request.headers['content-length'])
            json_string = cherrypy.request.body.read(length).decode("utf-8")
            update = telebot.types.Update.de_json(json_string)
            self.bot.process_new_updates([update])
            return ''
        else:
            raise cherrypy.HTTPError(403)

    def webhook_start(self):
        from cron import schedule_cron
        try:
            self.message_handlers()
            self.bot.remove_webhook()
            self.bot.set_webhook(url=self.config['WEBHOOK_URL_BASE'] + self.config['WEBHOOK_URL_PATH'],
                                 certificate=open(self.config['WEBHOOK_SSL_CERT'], 'r'))
            cherrypy.config.update({
                'server.socket_host': self.config['WEBHOOK_LISTEN'],
                'server.socket_port': self.config['WEBHOOK_PORT'],
                'server.ssl_module': 'builtin',
                'server.ssl_certificate': self.config['WEBHOOK_SSL_CERT'],
                'server.ssl_private_key': self.config['WEBHOOK_SSL_PRIV']
            })
            schedule_cron.sys_user_cron_start("pythonbot")
            schedule_cron.rkhunter_cron_start("pythonbot")
            schedule_cron.run_once()
            self.logger.info('Webhook is setting up')
            self.get_bot_commands()
            cherrypy.quickstart(Main(), self.config['WEBHOOK_URL_PATH'], {'/': {}})
        except cherrypy.CherryPyException:
            self.logger.exception('Webhook was not set up')
        finally:
            schedule_cron.close_cron_sys_user("pythonbot")
            schedule_cron.close_cron_rkhunter("pythonbot")

    def add_wireguard_user(self, name, ip_addr, keep_alive):
        try:
            name, ip_addr, pubkey, privkey, keep_alive, path = wireguard.UserManipulation.create_user(name, ip_addr,
                                                                                                      keep_alive)
            self.bd.insert_wireguard_user(name, pubkey, privkey, ip_addr, keep_alive, path)
            self.logger.info("wireguard user added")
            return path, ip_addr
        except:  # Application must go on any way regardless of errors
            self.logger.exception("Exception occurred while trying to add wireguard user")

    def del_wireguard_user(self, name):
        try:
            wireguard.UserManipulation.delete_user(name)
            self.bd.delete_wireguard_user(name)
            self.logger.info("wireguard user deleted")
        except:  # Application must go on any way regardless of errors
            self.logger.exception("Exception occurred while trying to delete wireguard user")

    @staticmethod
    def is_ip(ip):
        ip_parts = ip.split(".")
        if len(ip_parts) != 4:
            return False
        for part in ip_parts:
            if not int(part) or int(part) not in range(0, 256):
                return False
        return True

    def get_bot_commands(self, role=0):
        handlers = self.bot.message_handlers
        commands = []
        coco = operator.attrgetter('co_code', 'co_consts')
        for handler in handlers:
            if 'commands' in list(handler['filters']):
                for command in handler['filters']['commands']:
                    if 'func' in list(handler['filters']):
                        if coco(handler['filters']['func'].__code__) == \
                                coco((lambda x: self.is_logged_in(x)).__code__):
                            commands.append(["/" + command, "Могут использовать все"])
                        elif self.is_allowed(role, 'selectMe') and coco(handler['filters']['func'].__code__) == \
                                coco((lambda x: self.is_logged_in(x) and self.is_allowed(self.get_role(x),
                                                                                         'selectMe')).__code__):
                            commands.append(["/" + command, "Для использования нужно иметь права selectMe"])
                        elif self.is_allowed(role, 'select') and coco(handler['filters']['func'].__code__) == \
                                coco((lambda x: self.is_logged_in(x) and self.is_allowed(self.get_role(x),
                                                                                         'select')).__code__):
                            commands.append(["/" + command, "Для использования нужно иметь права select"])
                        elif self.is_allowed(role, 'updateMe') and coco(handler['filters']['func'].__code__) == \
                                coco((lambda x: self.is_logged_in(x) and self.is_allowed(self.get_role(x),
                                                                                         'updateMe')).__code__):
                            commands.append(["/" + command, "Для использования нужно иметь права updateMe"])
                        elif self.is_allowed(role, 'update') and coco(handler['filters']['func'].__code__) == \
                                coco((lambda x: self.is_logged_in(x) and self.is_allowed(self.get_role(x),
                                                                                         'update')).__code__):
                            commands.append(["/" + command, "Для использования нужно иметь права update"])
                        elif self.is_allowed(role, 'delete') and coco(handler['filters']['func'].__code__) == \
                                coco((lambda x: self.is_logged_in(x) and self.is_allowed(self.get_role(x),
                                                                                         'delete')).__code__):
                            commands.append(["/" + command, "Для использования нужно иметь права delete"])
                        elif self.is_allowed(role, 'insert') and coco(handler['filters']['func'].__code__) == \
                                coco((lambda x: self.is_logged_in(x) and self.is_allowed(self.get_role(x),
                                                                                         'insert')).__code__):
                            commands.append(["/" + command, "Для использования нужно иметь права insert"])
                        elif self.is_allowed(role, 'delete') and self.is_allowed(role, 'insert') and coco(
                                handler['filters']['func'].__code__) == \
                                coco((lambda x: self.is_logged_in(x) and self.is_allowed(self.get_role(x),
                                                                                         'delete') and self.is_allowed(
                                    self.get_role(x), 'insert')).__code__):
                            commands.append(["/" + command, "Для использования нужно иметь права insert and delete"])
                    else:
                        commands.append(["/" + command, "Могут использовать все"])
            # self.logger.info(handler)
        # self.logger.info(commands)
        return commands


def bot_start():
    m = Main()
    m.webhook_start()


if __name__ == "__main__":
    bot_start()
