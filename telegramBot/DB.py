import psycopg2
import yaml
import logging.config


class DataBase:
    with open('settings.yaml') as f:
        config = yaml.safe_load(f)

    logging_config = config['logging_config']
    logging.config.dictConfig(logging_config)
    logger = logging.getLogger('main_logger')

    def __init__(self, host=config['base_host'],
                 port=config['base_port'], dbname=config['base_dbname'], user=config['base_user'],
                 password=config['base_password']):
        self.host = host
        self.port = port
        self.dbname = dbname
        self.user = user
        self.password = password
        self.connection = psycopg2.connect(host=self.host, port=self.port, dbname=self.dbname, user=self.user,
                                           password=self.password)
        self.cursor = self.connection.cursor()
        self.logger.info('BD has been initialized')

    def user_pass(self):
        try:
            with self.connection:
                self.cursor.execute('select login, passwd, role from users')
                login_pass = self.cursor.fetchall()
                self.logger.info('Users have been parsed')
                if login_pass:
                    login_pass_group_list = [[step[0], bytes(step[1]), int(step[2])] for step in login_pass]
                    return login_pass_group_list
                return []
        except psycopg2.Error:
            self.logger.exception('Exception occurred during parsing users')
            return []

    def get_sys_user(self):
        try:
            with self.connection:
                self.cursor.execute('select name, groups from users_sys')
                login_group = self.cursor.fetchall()
                self.logger.info('System users have been parsed')
                if login_group:
                    login_group_dict = dict(zip([step[0] for step in login_group], [list(step[1].split())
                                                                                    for step in login_group]))
                    return login_group_dict
                return {}
        except psycopg2.Error:
            self.logger.exception('Exception occurred during parsing system users')
            return {}

    def insert_sys_user(self, name, groups, time):
        try:
            with self.connection:
                self.cursor.execute('insert into users_sys (id, name, groups, time_find) values '
                                    '(%s, %s, %s, %s)',
                                    (self.min_avaible_id('users_sys'), name, " ".join(groups), time))
                self.logger.info('New system users have been inserted')
                return True
        except psycopg2.Error:
            self.logger.exception('Exception occurred during updating users')
            raise

    def update_sys_user(self, name, groups, time):
        try:
            with self.connection:
                self.cursor.execute('update users_sys set groups = %s, time_find = %s '
                                    'where name = %s', (" ".join(groups), time, name))
                self.logger.info('System users have been updated')
                return True
        except psycopg2.Error:
            self.logger.exception('Exception occurred during updating system users')
            raise

    def delete_sys_user(self, name):
        try:
            with self.connection:
                self.cursor.execute('delete from users_sys '
                                    'where name = %s', (name,))
                self.logger.info('Old system users have been deleted')
                return True
        except psycopg2.Error:
            self.logger.exception('Exception occurred during deleting system users')
            raise

    def insert_wireguard_user(self, username, pubkey, prkey, ip_add, persistentkeepalive, file_path):
        try:
            with self.connection:
                self.cursor.execute('insert into wireguard '
                                    '(id, username, pubkey, prkey, ip_add, persistentkeepalive, file_path) '
                                    'values (%s, %s, %s, %s, %s, %s, %s)',
                                    (self.min_avaible_id('wireguard'), username, pubkey,
                                     prkey, str(ip_add), persistentkeepalive, file_path))
                self.logger.info('New system users have been inserted')
                return True
        except psycopg2.Error:
            self.logger.exception('Exception occurred during updating users')
            raise

    def delete_wireguard_user(self, name):
        try:
            with self.connection:
                self.cursor.execute('delete from wireguard '
                                    'where username = %s', (name,))
                self.logger.info('NWireguard user has been deleted')
                return True
        except psycopg2.Error:
            self.logger.exception('Exception occurred during deleting wireguard user')
            raise

    def get_permissions(self):
        try:
            with self.connection:
                self.cursor.execute('select description from roles')
                desc = self.cursor.fetchall()
                self.logger.info('Permissions have been parsed')
                if desc:
                    return desc
                return []
        except psycopg2.Error:
            self.logger.exception('Exception occurred during parsing permissions')
            raise

    def insert_user(self, login, password, group):
        try:
            with self.connection:
                self.cursor.execute('insert into users (id, login, passwd, wg_id, role) values '
                                    '(%s, %s, %s, %s, %s)',
                                    (self.min_avaible_id('users'), login, password,
                                     self.min_avaible_id('users'), group))
                self.logger.info('User have been inserted')
                return True
        except psycopg2.Error:
            self.logger.exception('Exception occurred during inserting users')
            raise

    def update_user(self, login, group):
        try:
            with self.connection:
                self.cursor.execute('update users set role = %s '
                                    'where login = %s', (group, login))
                self.logger.info('Users have been updated')
                return True
        except psycopg2.Error:
            self.logger.exception('Exception occurred during updating users')
            raise

    def min_avaible_id(self, table):
        try:
            all_ids = set(range(self.max_id(table) + 2))
            self.cursor.execute('select id from {}'.format(table))
            ids = set([step[0] for step in self.cursor.fetchall()])
            return min(all_ids - ids)
        except psycopg2.Error:
            self.logger.exception('No users in database')
        return 0

    def max_id(self, table):
        try:
            self.cursor.execute('select max(id) from {}'.format(table))
            result = self.cursor.fetchone()[0]
            self.logger.info('No users in database')
            if result:
                return result
        except psycopg2.Error:
            self.logger.exception('No users in database')
        return 0

    def del_user(self, username):
        try:
            with self.connection:
                self.cursor.execute('delete from users where login = %s', (username,))
                self.logger.info('User %s has been deleted', (username,))
                return True
        except psycopg2.Error:
            self.logger.exception('Exception occurred during deleting users')
            raise

    def start(self, host=config['base_host'],
              port=config['base_port'], dbname=config['base_dbname'], user=config['base_user'],
              password=config['base_password']):
        try:
            self.connection = psycopg2.connect(host=host, port=port, dbname=dbname, user=user,
                                               password=password)
            self.cursor = self.connection.cursor()
            self.logger.info('Connected successfully')
            return True
        except psycopg2.Error:
            self.logger.exception('Exception occurred during connecting')
            raise

    def stop(self):
        try:
            self.cursor.close()
            self.connection.close()
            self.logger.info('Stopped successfully')
            return True
        except psycopg2.Error:
            self.logger.exception('Exception occurred during stopping connection')
            raise

    def select_users(self):
        headers = ['id', 'login', 'passwd', 'wg_id', 'role']
        try:
            with self.connection:
                self.cursor.execute('select * from users order by id asc')
                users = self.cursor.fetchall()
        except psycopg2.Error:
            users = []
        return users, headers, 'users'

    # needed to export stored data to pdf
    def select_roles(self):
        headers = ['id_role', 'access', 'decription']
        try:
            with self.connection:
                self.cursor.execute('select * from roles order by id_role asc')
                users = self.cursor.fetchall()
        except psycopg2.Error:
            users = []
        return users, headers, 'roles'

    def select_system_users(self):
        headers = ['id', 'name', 'groups', 'time_find']
        try:
            with self.connection:
                self.cursor.execute('select * from users_sys order by id asc')
                users = self.cursor.fetchall()
        except psycopg2.Error:
            users = []
        return users, headers, 'system users'

    def select_wg_users(self):
        headers = ['id', 'username', 'pubkey', 'prkey', 'ip_add', 'persistentkeepalive', 'file_path']
        try:
            with self.connection:
                self.cursor.execute('select * from wireguard order by id asc')
                users = self.cursor.fetchall()
        except psycopg2.Error:
            users = []
        return users, headers, 'wireguard users'
