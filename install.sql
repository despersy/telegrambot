
CREATE TABLE public.roles (
    id_role integer NOT NULL,
    access text NOT NULL,
    description text NOT NULL
);


ALTER TABLE public.roles OWNER TO postgres;

CREATE SEQUENCE public.roles_id_role_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;

ALTER TABLE public.roles_id_role_seq OWNER TO postgres;
ALTER SEQUENCE public.roles_id_role_seq OWNED BY public.roles.id_role;

CREATE TABLE public.users (
    id integer NOT NULL,
    login character varying(50) NOT NULL,
    passwd text NOT NULL,
    wg_id integer NOT NULL,
    role integer NOT NULL
);

ALTER TABLE public.users OWNER TO postgres;

CREATE TABLE public.users_sys (
    id integer NOT NULL,
    name text NOT NULL,
    groups text NOT NULL,
    time_find timestamp without time zone NOT NULL
);

ALTER TABLE public.users_sys OWNER TO postgres;

CREATE TABLE public.wireguard (
    id integer NOT NULL,
    username character varying(50) NOT NULL,
    pubkey text NOT NULL,
    prkey text NOT NULL,
    ip_add character varying(27) NOT NULL,
    persistentkeepalive smallint DEFAULT 30 NOT NULL
);

ALTER TABLE public.wireguard OWNER TO postgres;

ALTER TABLE ONLY public.roles ALTER COLUMN id_role SET DEFAULT nextval('public.roles_id_role_seq'::regclass);

COPY public.roles (id_role, access, description) FROM stdin;
1	selectMe	can select only his own data
2	select	can select all data
3	updateMe&selectMe	can select and update his own data
4	select&update	can select and update all data
5	select&insert	can select and insert all data
6	select&insert&update	can select, insert and update all data
7	select&update&delete	can select, update and delete all data
8	select&update&delete&insert	grants all permissions
\.

SELECT pg_catalog.setval('public.roles_id_role_seq', 8, true);

ALTER TABLE ONLY public.roles
    ADD CONSTRAINT roles_pkey PRIMARY KEY (id_role);

ALTER TABLE ONLY public.users
    ADD CONSTRAINT users_login_key UNIQUE (login);

ALTER TABLE ONLY public.users
    ADD CONSTRAINT users_pkey PRIMARY KEY (id);

ALTER TABLE ONLY public.users_sys
    ADD CONSTRAINT users_sys_name_key UNIQUE (name);

ALTER TABLE ONLY public.users_sys
    ADD CONSTRAINT users_sys_pkey PRIMARY KEY (id);

ALTER TABLE ONLY public.wireguard
    ADD CONSTRAINT wireguard_ip_add_key UNIQUE (ip_add);

ALTER TABLE ONLY public.wireguard
    ADD CONSTRAINT wireguard_pkey PRIMARY KEY (id);

ALTER TABLE ONLY public.wireguard
    ADD CONSTRAINT wireguard_username_key UNIQUE (username);

GRANT ALL ON TABLE public.roles TO pythonbot;

GRANT ALL ON TABLE public.users TO pythonbot;

GRANT ALL ON TABLE public.users_sys TO pythonbot;

GRANT ALL ON TABLE public.wireguard TO pythonbot;


