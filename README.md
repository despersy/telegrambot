# Telegram bot
## what can it do?
1. Manage your wireguard users without need to change conf files every time you would need it.
2. get sshd logs from auth.log file.
3. checking rootkits via rkhunter utility
4. managing fail2ban functions, without need to connect to server via ssh
5. listing your system users with time there was found on your system

## Install
1. ```git clone https://gitlab.com/despersy/telegrambot.git```
2. ```cd telegrambot```
3. ```
   openssl genrsa -out webhook_pkey.pem 2048
   openssl req -new -x509 -days 3650 -key webhook_pkey.pem -out webhook_cert.pem
   ```
4. We will be asked to enter some information about ourselves.
   If you don't want to enter anything, put a period. BUT! IMPORTANT! 
   When you get to the suggestion to enter a Common Name, 
   you should write the IP address of the server on which the bot will be launched. 
5. If you later want to move the server, you not only have to reconnect it in the bot with 
   /reconnect, but also have to recreate the certificates
6. Now go to Telegram, search for BotFather and start it (or ```/start```)
7. ```/newbot```
8. ```name``` (Not unique)
9. ```"@usernameBot or @username_bot"``` (unique name)
10. Return to terminal
11. ```python3 install.py check-libs```. run first stage to install all needed libs
12. ```sudo python3 install.py install```. run second stage to create user and give him rights to work with databases and wireguard
13. ```
    sudo -u postgres psql -c 'create database pbot;'
    sudo -u postgres psql -c 'create user pythonbot;'
    ```
    create database pbot with user pythonbot
14. ```sudo -u postgres psql pbot < install.sql``` after this you must have already installed database with user.
15. ```sudo -u postgres psql```
16. ```
    \c pbot
    insert into users(login, passwd, wg_id, role)
    values('yourlogin','yourpassword', 1, 8);
    ``` 
    this line creating your first user with administration rights

17. ```sudo python3 main.py create``` to create service (only first time need)
18. ```python3 main.py enable``` to enable service (every time you changed sth in code)
19. ```python3 main.py start``` to start service 
20. ```python3 main.py restart``` to restart service 
21. ```python3 main.py stop``` to stop service
22. ```python3 main.py stats``` to view status of service
23. Go to Telegram, find your bot with ```@usernameBot``` and login with ```'yourlogin','yourpassword'```
## Additionally
You can add these hints for commands in telegram to make your and your users life easier.
1. Go to BotFather and write ```/setcommands```. Select your bot and then just copy-paste following:
```
login - username password - Вход в аккаунт
logout - - Выход из аккаунта
register - username password role(digit) ip_address=rand_ip keep_alive=25 - Регистрация нового пользователя
delete_user - username - Удаление существующего зарегистрированного пользователя
clear_logged - - Выход из аккаунта всех пользователей
export_as_pdf - - PDF файл со всеми данными из БД
show_ssh - - Последние 20 логов ssh
show_rkhunter_log - Последние 20 логов rkhunter
find_ip_fail2ban - find_by, ip_add - Забанен ли ip в системе мониторинга
show_users - - Показывает всех зарегистрированных пользователей
show_system_users - - Показывает всех пользователей сервера
show_wireguard_users - - Показывает всех пользователей wireguard
show_logged_users - - Показывает всех пользователей, которые зашли в аккаунт
show_my_info - - Показывает всю информацию о тебе
reconnect - host, port, dbname, user, password - Присоединение к серверу с выбранными параметрами (или к уже соединённому при отсутствии параметров)
start - - Подсказка по текущим возможностям пользователя
edit_user - username role - Изменить уровень разрешений пользователя
edit_log_level - level - Изменить минимальный уровень логирования
edit_log_chat - chat - Изменить id чата, в котором будут выведены логи работы бота.
```