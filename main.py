import sys
from service_master import ServiceMaster

if __name__ == '__main__':
    try:
        stage = sys.argv[1]
        if stage == 'start':
            ServiceMaster.start()
        elif stage == 'stop':
            ServiceMaster.stop()
        elif stage == 'status':
            ServiceMaster.status()
        elif stage == 'restart':
            ServiceMaster.restart()
        elif stage == 'enable':
            ServiceMaster.enable()
        elif stage == 'create':
            ServiceMaster.create_service()
        else:
            print("commands: create(with sudo) enable start status restart stop")
    except Exception as err:
        print("commands: create(with sudo) enable start status restart stop")
        raise
