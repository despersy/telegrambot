from crontab import CronTab
import subprocess
import os


def close_cron_sys_user(user):
    cron = CronTab(user=user)
    cron.remove_all(comment='pythonbot sys_users parse')
    cron.write()


def sys_user_cron_start(user):
    cron = CronTab(user=user)
    job = cron.new("/usr/bin/python3 {}/cron/sys_users.py".format(os.getcwd()), comment='pythonbot sys_users parse')
    job.day.every(1)
    cron.env['HOME'] = os.getcwd()
    cron.env['PATH'] = "/usr/bin:/bin:/usr/bin/python:/usr/bin/python3"
    cron.write()


def close_cron_rkhunter(user):
    cron = CronTab(user=user)
    cron.remove_all(comment='pythonbot rkhunter start')
    cron.write()


def rkhunter_cron_start(user):
    cron = CronTab(user=user)
    job = cron.new("/usr/bin/python3 {}/cron/rkhunter_cron.py".format(os.getcwd()), comment='pythonbot rkhunter start')
    job.day.every(1)
    cron.env['HOME'] = os.getcwd()
    cron.env['PATH'] = "/usr/bin:/bin:/usr/bin/python:/usr/bin/python3"
    cron.write()


def run_once():
    subprocess.call('run-parts /etc/cron.daily',
                    shell=True,
                    stdin=None)
