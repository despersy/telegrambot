import sys
import os

sys.path.insert(1, os.getcwd())
from OSfuncs import get_users
from telegramBot.DB import DataBase

bd = DataBase()
users_dict_new, time = get_users.create_dict()
users_dict_old = bd.get_sys_user()
set_new = set(users_dict_new.keys())
set_old = set(users_dict_old.keys())
to_insert = set_new - set_old
to_update = set_new & set_old
to_delete = set_old - set_new

for step in to_insert:
    bd.insert_sys_user(step, users_dict_new[step], time)
for step in to_update:
    if users_dict_new[step] != users_dict_old[step]:
        bd.update_sys_user(step, users_dict_new[step], time)
for step in to_delete:
    bd.delete_sys_user(step)
