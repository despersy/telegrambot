import subprocess
import os
import pathlib
from OSfuncs.wireguard import UserManipulation
from ipaddress import ip_interface


class UserCreation:
    @staticmethod
    def create_user(passwd):
        create_gorup = subprocess.Popen('sudo groupadd adm',
                                    shell=True,
                                    stdout=subprocess.PIPE)
        create_gorup.wait()
        create_user = subprocess.Popen('sudo useradd pythonbot -R `pwd` -p {passw} -G adm'.format(passw=passwd),
                                        shell=True,
                                        stdout=subprocess.PIPE)
        create_user.wait()

    @staticmethod
    def give_sudo_rights():
        with open('/etc/sudoers', 'a') as file:
            file.write('pythonbot   ALL=(root) NOPASSWD: /usr/bin/rkhunter, '
                       '/usr/bin/fail2ban-client, /usr/bin/openssl, '
                       '/usr/bin/apt, /usr/bin/wg pubkey, /usr/bin/wg genkey')

    @staticmethod
    def change_fail2ban_mod():
        if not os.path.exists('/var/lib/fail2ban/fail2ban.sqlite3'):
            pathlib.Path("/var/lib/fail2ban/").mkdir(parents=True, exist_ok=True)
            os.replace('telegrambot/rootInstall/fail2ban.sqlite3',
                       '/var/lib/fail2ban/fail2ban.sqlite3')  # change path by removing telegrambot
        run_fail2ban = subprocess.Popen('sudo systemctl start fail2ban',
                                        shell=True,
                                        stdout=subprocess.PIPE)
        run_fail2ban.wait()
        en_fail2ban = subprocess.Popen('sudo systemctl enable fail2ban',
                                       shell=True,
                                       stdout=subprocess.PIPE)
        en_fail2ban.wait()
        change_group = subprocess.Popen('sudo chown root:adm /var/lib/fail2ban/fail2ban.sqlite3',
                                        shell=True,
                                        stdout=subprocess.PIPE)
        change_group.wait()
        change_rights = subprocess.Popen('sudo chmod 640 /var/lib/fail2ban/fail2ban.sqlite3',
                                         shell=True,
                                         stdout=subprocess.PIPE)
        change_rights.wait()

    @staticmethod
    def change_mods_wireguard():
        change_rights = subprocess.Popen('sudo chmod 660 -R /etc/wireguard',
                                         shell=True,
                                         stdout=subprocess.PIPE)
        change_rights.wait()
        change_rights = subprocess.Popen('sudo chown root:adm -R /etc/wireguard',
                                         shell=True,
                                         stdout=subprocess.PIPE)
        change_rights.wait()

    @staticmethod
    def enable_port_forward():
        enable_port_forward = subprocess.Popen('sudo sh -c  "echo net.ipv4.ip_forward=1 >> /etc/sysctl.conf"',
                                               shell=True,
                                               stdout=subprocess.PIPE)
        enable_port_forward.wait()

    @staticmethod
    def create_wg_config():
        ip_add = input('enter ip address of your vpn server(ip/mask): ')
        try:
            ip_interface(ip_add)
            port = 65200  # input('enter port: ')
            UserManipulation.create_server_config(ip_add, port)
        except ValueError:
            print('wrong net addr')
            UserCreation.create_wg_config()


def __init__():
    print('now I need to create user pythonbot with your password(same will user for database)')
    passwd = input('enter passwd: ')
    UserCreation.create_user(passwd)
    UserCreation.give_sudo_rights()
    UserCreation.change_fail2ban_mod()
    ans = input('create wireguard config? [y/n]')
    if ans == 'y':
        UserCreation.create_wg_config()
    UserCreation.change_mods_wireguard()
    ans = input('enable port forward(needed to subnet vpn) [y/n]')
    if ans == 'y':
        UserCreation.enable_port_forward()
    return passwd
