import os
import subprocess


class ServiceMaster:
    @staticmethod
    def create_service():
        with open("/etc/systemd/system/pythonbot.service", "w") as file:
            file.write("[Unit]\n"
                       "Description=Telegram python bot\n"
                       "After=multi-user.target\n"
                       "\n"
                       "[Service]\n"
                       "WorkingDirectory={}\n"
                       "User=pythonbot\n"
                       "Type=simple\n"
                       "Restart=always\n"
                       "StartLimitInterval=600\n"
                       "ExecStart=/usr/bin/python3 {}/telegramBot/bot_master.py\n"
                       "\n"
                       "[Install]\n"
                       "WantedBy=multi-user.target\n".format(os.getcwd(), os.getcwd()))

    @staticmethod
    def enable():
        subprocess.call('sudo systemctl daemon-reload',
                        shell=True,
                        stdin=None)
        subprocess.call('sudo systemctl enable pythonbot.service',
                        shell=True,
                        stdin=None)

    @staticmethod
    def start():
        subprocess.call('sudo systemctl start pythonbot.service',
                        shell=True,
                        stdin=None)

    @staticmethod
    def restart():
        subprocess.call('sudo systemctl restart pythonbot.service',
                        shell=True,
                        stdin=None)

    @staticmethod
    def stop():
        subprocess.call('sudo systemctl stop pythonbot.service',
                        shell=True,
                        stdin=None)

    @staticmethod
    def status():
        subprocess.call('sudo systemctl status pythonbot.service',
                        shell=True,
                        stdin=None)
