import re


def parse_log():
    file = open('/var/log/auth.log', 'r')
    lines = file.readlines()
    re_failed = re.compile('sshd.*((authentication failure)|'
                           '(Failed password for)|'
                           '(Invalid user deploy)|)')
    re_accepted = re.compile('sshd.*((Accepted password for)|'
                             '(session opened for user))')
    # ssh connections that was denied by server by some reasons
    # if server opened records may be a lot
    filtered_data_fail = [line.strip() for line in lines if re.search(re_failed, line)]
    # ssh connections that connected successfully
    filtered_data_accepted = [line.strip() for line in lines if re.search(re_accepted, line)]

    return filtered_data_fail, filtered_data_accepted
