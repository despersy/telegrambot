import subprocess
import re


# needed to start command rkhunter
def rkhunter_start():
    checking = subprocess.Popen('sudo rkhunter --check --noappend-log -q -l /var/log/rkhunter.log',
                                shell=True,
                                stdout=subprocess.PIPE)
    checking.wait()


def parse_log() -> list:
    file = open('/var/log/rkhunter.log', 'r')
    lines = file.readlines()
    re_warnings = re.compile('(system checks took)|'
                             '(Rootkits|rootkits)|'
                             '(Files|files)|'
                             '(configuration option)|'
                             '([А-я].{1}\D[а-я]{3}'
                             '\s[0-9]{2}'
                             '\s[0-9]{2}:[0-9]{2}:[0-9]{2}'
                             '\s[A-Z]{3}\s[0-9]{4}|)'
                             '(Warning)|'
                             '(modification time(:|\s:)'
                             '\s[0-9]*'
                             '\s+[(][0-9]{2}-[а-я]{3}-[0-9]{4}'
                             '\s[0-9]{2}:[0-9]{2}:[0-9]{2}[)])')
    filtered_data = [line.strip() for line in lines if re.search(re_warnings, line)]

    return filtered_data
