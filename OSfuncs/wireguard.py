# -*- coding: utf-8 -*-
import yaml
import subprocess
import ipaddress
import requests
import os


class Yml:
    @staticmethod
    def read_yml():
        with open('config.yml', 'r') as file:
            data = yaml.safe_load(file)
            return data

    @staticmethod
    def write_yml(data):
        with open('config.yml', 'w') as file:
            yaml.dump(data, file)

    @staticmethod
    def get_public_ip() -> str:
        endpoint = 'https://ipinfo.io/json'
        response = requests.get(endpoint, verify=True)

        if response.status_code != 200:
            return 'network is unreachable'

        data = response.json()
        return data['ip']


# read data from wireguard configuration file
class ReadWg:
    @staticmethod
    def clear_server_data_wg(lines) -> dict:
        if lines == '':
            file = open('/etc/wireguard/server.conf', 'r')
            lines = file.readlines()
        return_data = {}
        for line in lines:
            if line.strip() != "" and line.strip() != '[Interface]' and line.strip() != '[Peer]':
                temp = line.strip().split('=', 1)
                temp[0], temp[1] = temp[0].strip(), temp[1].strip()
                return_data.update({temp[0]: temp[1]})
            elif line.strip() == '[Peer]':
                break
        with open('/etc/wireguard/keys/server_public_key') as file:
            pubkey = file.readlines()[0].strip()
            return_data['PublicKey'] = pubkey
        return_data['endpoint'] = Yml.get_public_ip()
        Yml.write_yml(return_data)
        return return_data

    @staticmethod
    def clear_user_data_wg(lines) -> list:
        if lines == '':
            file = open('/etc/wireguard/server.conf', 'r')
            lines = file.readlines()
        return_data = []
        users_start = False
        return_data_count = -1
        tempdict = {}
        for line in lines:
            if line.strip() == '[Peer]':
                return_data.append([])
                return_data_count = return_data_count + 1
                users_start = True
            elif users_start and line.strip() != '[Peer]' and line.strip() != '':
                temp = line.strip().split('=', 1)
                if '#' in temp[0]:
                    temp[0] = temp[0].strip().split('#')[1].strip()
                else:
                    temp[0] = temp[0].strip()
                temp[1] = temp[1].strip()
                tempdict[temp[0]] = temp[1]
                return_data[return_data_count] = tempdict.copy()
        return return_data

    @staticmethod
    def parse_wg():
        file = open('/etc/wireguard/server.conf', 'r')
        lines = file.readlines()
        users = ReadWg.clear_user_data_wg(lines)
        server = ReadWg.clear_server_data_wg(lines)
        return users, server


class UserManipulation:
    @staticmethod
    def create_keys(name) -> list:
        try:
            get_private_proc = subprocess \
                .Popen('sudo wg genkey | tee /etc/wireguard/keys/{name}_private_key \
                             | wg pubkey > /etc/wireguard/keys/{name}_public_key'
                       .format(name=name),
                       shell=True,
                       stdout=subprocess.PIPE)
            get_private_proc.wait()
            open('/etc/wireguard/keys/{name}_private_key'.format(name=name))
            open('/etc/wireguard/keys/{name}_public_key'.format(name=name))
        except FileNotFoundError:
            subprocess.call('mkdir -p /etc/wireguard/keys',
                            shell=True,
                            stdin=None)
        finally:
            get_private_proc = subprocess \
                .Popen('sudo wg genkey | tee /etc/wireguard/keys/{name}_private_key \
                 | wg pubkey > /etc/wireguard/keys/{name}_public_key'
                       .format(name=name),
                       shell=True,
                       stdout=subprocess.PIPE)
            get_private_proc.wait()
            private_file = open('/etc/wireguard/keys/{name}_private_key'.format(name=name))
            public_file = open('/etc/wireguard/keys/{name}_public_key'.format(name=name))
        priv_key = private_file.readlines()[0].strip()
        pub_key = public_file.readlines()[0].strip()
        return [pub_key, priv_key]

    @staticmethod
    def create_user(name, ip_addr, persistent_keep_alive):
        keys = UserManipulation.create_keys(name)
        if ip_addr == '':
            try:
                last_user = ReadWg.parse_wg()[0][-1].get('AllowedIPs')
            except IndexError:
                last_user = Yml.read_yml()['Address']
            last_user = ipaddress.ip_address(last_user.split('/')[0])
            ip_addr = last_user + 1
        with open('/etc/wireguard/server.conf', 'a') as file:
            file.write('\n[Peer]\n' +
                       '# name = {name}\n'.format(name=name) +
                       'AllowedIPs = {addr}/32\n'.format(addr=ip_addr) +
                       'PublicKey = {privkey}\n'.format(privkey=keys[0]) +
                       'PersistentKeepalive = {time}\n'.format(time=persistent_keep_alive))
        path = UserManipulation.create_user_config(name, ip_addr, keys[1], persistent_keep_alive)
        subprocess.Popen('sudo systemctl restart wg-quick@server',
                         shell=True,
                         stdout=subprocess.PIPE)
        return name, ip_addr, keys[0], keys[1], persistent_keep_alive, path

    @staticmethod
    def delete_user(name):
        users = ReadWg.parse_wg()[0]
        users = [user for user in users if user.get('name') != name]
        lines = []
        with open('/etc/wireguard/server.conf', 'r') as file:
            for line in file.readlines():
                if line.strip() == '[Peer]':
                    break
                elif line.strip() != '':
                    lines.append(line)
        with open('/etc/wireguard/server.conf', 'w') as file:
            file.write(''.join([line for line in lines[:len(lines)]]))
            for user in users:
                if user.get('PersistentKeepalive') != None:
                    file.write('[Peer]\n' +
                               '# name = {name}\n'.format(name=user.get('name')) +
                               'PublicKey = {pubkey}\n'.format(pubkey=user.get('PublicKey')) +
                               'AllowedIPs = {ips}\n'.format(ips=user.get('AllowedIPs')) +
                               'PersistentKeepalive = {time}\n'.format(time=user.get('PersistentKeepalive')))
                else:
                    file.write('[Peer]\n' +
                               '# name = {name}\n'.format(name=user.get('name')) +
                               'PublicKey = {pubkey}\n'.format(pubkey=user.get('PublicKey')) +
                               'AllowedIPs = {ips}\n'.format(ips=user.get('AllowedIPs')) +
                               'PersistentKeepalive = 30\n')
        subprocess.Popen('sudo systemctl restart wg-quick@server',
                         shell=True,
                         stdout=subprocess.PIPE)
        try:
            os.remove('/etc/wireguard/users/{name}.conf'.format(name=name))
        except OSError:
            pass

    @staticmethod
    def create_user_config(name, ip_addr, pubkey, persistent) -> str:
        server_data = Yml.read_yml()
        with open('/etc/wireguard/users/{name}.conf'.format(name=name), 'w') as file:
            file.write('[Interface]\n' +
                       'Address = {ip_addr}/32\n'.format(ip_addr=ip_addr) +
                       'PrivateKey = {pub}\n'.format(pub=pubkey) +
                       '[Peer]\n' +
                       'PublicKey = {serv_pub}\n'.format(serv_pub=server_data['PublicKey']) +
                       'AllowedIPs = 0.0.0.0/0\n' +
                       'Endpoint = {endp}:{port}\n'.format(endp=server_data['endpoint'],
                                                           port=server_data['ListenPort']) +
                       'PersistentKeepalive = {time}\n'.format(time=persistent))
        return '/etc/wireguard/users/{name}.conf'.format(name=name)

    # used to write config for the first time
    @staticmethod
    def create_server_config(ip_addr, port):
        keys = UserManipulation.create_keys('server')
        with open('/etc/wireguard/server.conf', 'w') as file:
            file.write('[Interface]\n' +
                       'Address = {addr}\n'.format(addr=ip_addr) +
                       'PrivateKey = {privkey}\n'.format(privkey=keys[1]) +
                       'ListenPort = {port}\n'.format(port=port))
