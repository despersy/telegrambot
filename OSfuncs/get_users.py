import re
import subprocess
from datetime import datetime
import os
from stat import *


def parse_users() -> list:
    file = open('/etc/passwd', 'r')
    lines = file.readlines()
    re_users = re.compile("/bin/(sh|bash|zsh)")
    filtered_users = [line.strip() for line in lines if re.search(re_users, line.strip())]
    users = [user.split(':')[0] for user in filtered_users]
    return users


def get_groups(users) -> list:
    users_with_groups = []
    for user in users:
        checking = subprocess.check_output(['groups', user])
        users_with_groups.append(checking.strip().decode('utf-8'))

    groups = [user.split(':')[1].split(' ')[1:] for user in users_with_groups]
    return groups


# returns dictionary with key = user and value = groups
def create_dict():
    users = parse_users()
    groups = get_groups(users)
    users_dict = {}
    for i in range(len(users)):
        users_dict.update({users[i]: groups[i]})
    unix_time = os.stat('/etc/passwd').st_mtime
    time = datetime.fromtimestamp(unix_time).strftime('%Y-%m-%d %H:%M:%S')
    return users_dict, time


def __init__():
    create_dict()
