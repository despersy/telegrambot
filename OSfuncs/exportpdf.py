from fpdf import FPDF, HTMLMixin


class ExportPdf:
    class MyFPDF(FPDF, HTMLMixin):
        pass

    @staticmethod
    def to_pdf(args):
        pdf = ExportPdf.MyFPDF(orientation="landscape", format="A3")
        for arg in args:
            pdf.add_page()
            data = '<h1>{table_name}</h1>'.format(table_name=arg[2])
            if arg[0]:
                data += ExportPdf.generate_data(arg)
            else:
                data += '<table border="1" align="center" width="90%"><thead><tr>'
                for name in arg[1]:
                    data += '<th width="{width}%">{name}</th>'.format(name=name, width=100 // len(arg[1]))
                data += '</tr></thead><tbody><tr><td colspan={len}>no data</td></tr></tbody></table>'.format(
                    len=len(arg[1]))
            pdf.write_html(data)
        pdf.output('/var/log/report.pdf')

    @staticmethod
    def generate_data(arg):
        data = '<table border="1" align="center" width="100%"><thead><tr>'
        for name in arg[1]:
            data += '<th width="{width}%">{name}</th>'.format(name=name, width=100 // len(arg[1]))
        data += '</tr></thead>\n<tbody>'
        try:
            for row in arg[0]:
                data += '<tr>'
                for elem in row:
                    if isinstance(elem, memoryview):
                        data += '<td width="{width}%">{row}</td>'.format(
                            row='...{passw}...'.format(passw=elem.tobytes()[:8]), width=100 // len(arg[1]))
                    elif type(elem)==str and len(elem)>40:
                        data += '<td width="{width}%">{row}</td>'.format(
                            row='...{passw}...'.format(passw=elem[:8]), width=100 // len(arg[1]))
                    else:
                        data += '<td width="{width}%">{row}</td>'.format(row=elem, width=100 // len(arg[1]))
                data += '</tr>'
            data += '</tbody></table>'
        except IndexError:
            data += '<tr><td colspan="{len}">no data</td></tr></tbody></table>'.format(len=len(arg[1]))
        return data

    @staticmethod
    def __init__(db):
        users = db.select_users()
        roles = db.select_roles()
        system_users = db.select_system_users()
        wg_users = db.select_wg_users()
        ExportPdf.to_pdf([users, roles, system_users, wg_users])
