import sqlite3
import subprocess
from datetime import datetime


# returns ip and time when banned
def find_in_db(find_by, ip_add) -> list:
    db = sqlite3.connect('file:/var/lib/fail2ban/fail2ban.sqlite3?mode=ro', uri=True)
    cur = db.cursor()
    list_banned = cur.execute('select ip, timeofban, data from bans where jail=?', (find_by,))
    list_banned = list_banned.fetchall()
    db.close()
    result = []
    for ip in list_banned:
        if ip[0] == ip_add:
            result.append([ip[0], datetime.utcfromtimestamp(ip[1]).strftime('%Y-%m-%d %H:%M:%S')])
    result = False if result != [] else result
    return result


# ip_add - target ip address, jail_name - type of ban(nginx, sshd etc)
def ban(ip_add, jail_name):
    checking = subprocess.Popen('sudo fail2ban-client set {jail_name} banip {ip_add}'.format(
        jail_name=jail_name,
        ip_add=ip_add
    ),
                                shell=True,
                                stdout=subprocess.PIPE)
    checking.wait()


# ip_add - target ip address, jail_name - type of ban(nginx, sshd etc)
def unban(ip_add, jail_name):
    checking = subprocess.Popen('sudo fail2ban-client set {jail_name} unbanip {ip_add}'.format(
        jail_name=jail_name,
        ip_add=ip_add
    ),
                                shell=True,
                                stdout=subprocess.PIPE)
    checking.wait()


def __init__():
    # search by keyword like sshd, nginx etc
    find_by = "sshd"
    ip_add = "102.220.23.35"
    find_in_db(find_by, ip_add)
