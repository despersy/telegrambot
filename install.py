import subprocess

import service_master
import sys


# checking installed packages and python libraries
class CheckInstalled:
    @staticmethod
    def check_packages():
        list_installed = subprocess.Popen(['dpkg', '-l'],
                                          stdout=subprocess.PIPE).communicate()[0].decode('utf-8').split('\n')
        try:
            subprocess.Popen(['pip3', 'list'], stdout=subprocess.PIPE).communicate()[0].decode('utf-8').split('\n')
        except FileNotFoundError:
            print('seems pip3 not installed. Can I install it?')
            subprocess.call('sudo apt install -y python3-pip',
                            shell=True,
                            stdin=None)
        finally:
            list_python_installed = subprocess.Popen(['pip3', 'list'],
                                                     stdout=subprocess.PIPE
                                                     ).communicate()[0].decode('utf-8').split('\n')
        check = [{'sudo': False,
                  'python3-pip': False,
                  'fail2ban': False,
                  'sqlite3': False,
                  'rkhunter': False,
                  'postgresql': False,
                  'wireguard': False,
                  'openssl': False,
                  'net-tools': False,
                  'python3-requests': False,
                  'systemctl': False,
                  'python-crontab': False},
                 {'psycopg2-binary': False,
                  'pyTelegramBotAPI': False,
                  'PyYAML': False,
                  'CherryPy': False,
                  'pypdf': False,
                  'pypdf2': False,
                  'fpdf': False}]
        # run through linux packages
        for line in list_installed:
            for name, value in check[0].items():
                if name in line:
                    check[0][name] = True
        # run through python packages
        for line in list_python_installed:
            for name, value in check[1].items():
                if name in line:
                    check[1][name] = True
        # output result
        to_install = [[name for name, value in check[0].items() if not value],
                      [name for name, value in check[1].items() if not value]]
        for sub_check in check:
            for name, value in sub_check.items():
                if value:
                    print('package {package} installed correctly'
                          .format(package=name))
                else:
                    print('I need to install {package} to work correctly'
                          .format(package=name))
        CheckInstalled.installer(to_install)

    @staticmethod
    def installer(packages):
        if not packages[0] and packages[1]:
            Installers.install_python(packages[1])
        elif not packages[1] and packages[0]:
            Installers.install_linux(packages[0])
        elif packages[0] and packages[1]:
            Installers.install_linux(packages[0])
            Installers.install_python(packages[1])
        else:
            print('all packets and python libraries are installed')


# commands to install packages/libraries
class Installers:
    @staticmethod
    def install_python(packages):
        if len(packages) > 1:
            subprocess.call('pip3 install {name} --system'
                            .format(name=' '.join(packages)),
                            shell=True,
                            stdin=None)
        elif len(packages) == 1:
            subprocess.call('pip3 install {name} --system'
                            .format(name=packages),
                            shell=True,
                            stdin=None)
        else:
            print('no packages to install')
        subprocess.call('sudo pip3 install pyaml requests',
                        shell=True,
                        stdin=None)

    @staticmethod
    def install_linux(packages):
        if len(packages) > 1:
            subprocess.call('sudo apt install -y {packages}'
                            .format(packages=' '.join(packages)),
                            shell=True,
                            stdin=None)
        elif len(packages) == 1:
            subprocess.call('sudo apt install -y {packages}'
                            .format(packages=packages),
                            shell=True,
                            stdin=None)
        else:
            print('no packages to install')


if __name__ == '__main__':
    print("the installation path will be the way the program works, as it works with Jetbrains in linux")
    try:
        stage = sys.argv[1]
        if stage == 'check-libs':
            CheckInstalled.check_packages()
        if stage == 'install':
            import yaml_master
            import rootInstall

            service_master.ServiceMaster.create_service()
            passwd = rootInstall.__init__()
            yaml_master.YamlMaster(passwd).create_yaml()
        else:
            print(
                'python3 install.py install - install dependincies for tgbot\n'
                'python3 install.py check-libs - check installed libraries')
    except IndexError as err:
        print(
            'python3 install.py install - install dependincies for tgbot\n'
            'python3 install.py check-libs - check installed libraries')
