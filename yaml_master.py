import logging.config
import subprocess
from OSfuncs import wireguard
import yaml
import os
from subprocess import Popen, PIPE
from re import search
from re import compile as re_compile


class YamlMaster:

    def __init__(self, passw=""):
        if os.path.exists('settings.yaml'):
            with open('settings.yaml', 'r') as f:
                self.config = yaml.safe_load(f)
            self.logging_config = self.config['logging_config']
            logging.config.dictConfig(self.logging_config)
            self.logger = logging.getLogger('main_logger')

            self.token = self.config['token']
            self.WEBHOOK_HOST = self.config['WEBHOOK_HOST']
            self.WEBHOOK_PORT = self.config['WEBHOOK_PORT']
            self.WEBHOOK_LISTEN = self.config['WEBHOOK_LISTEN']
            self.WEBHOOK_PATH = os.getcwd()
            self.WEBHOOK_SSL_CERT = self.config['WEBHOOK_SSL_CERT']
            self.WEBHOOK_SSL_PRIV = self.config['WEBHOOK_SSL_PRIV']
            self.WEBHOOK_URL_BASE = self.config['WEBHOOK_URL_BASE']
            self.WEBHOOK_URL_PATH = self.config['WEBHOOK_URL_PATH']
            self.base_host = self.config['base_host']
            self.base_port = self.config['base_port']
            self.base_dbname = self.config['base_dbname']
            self.base_user = self.config['base_user']
            self.base_password = self.config['base_password']
            self.level = self.config['logging_config']['loggers']['main_logger']['level']
            self.chat = self.config['logging_config']['handlers']['telegram_handler']['chat_id']
        else:
            self.token = input("Input your bot token:")
            self.WEBHOOK_HOST = wireguard.Yml.get_public_ip()
            self.WEBHOOK_PORT = 8443
            self.WEBHOOK_LISTEN = '0.0.0.0'
            self.WEBHOOK_PATH = os.getcwd()
            self.WEBHOOK_SSL_CERT = '{}/webhook_cert.pem'.format(self.WEBHOOK_PATH)
            self.WEBHOOK_SSL_PRIV = '{}/webhook_pkey.pem'.format(self.WEBHOOK_PATH)
            self.WEBHOOK_URL_BASE = "https://{}:{}".format(self.WEBHOOK_HOST, self.WEBHOOK_PORT)
            self.WEBHOOK_URL_PATH = "/{}/".format(self.token, )
            self.base_host = '/var/run/postgresql'
            self.base_port = self.get_db_port()
            self.base_dbname = 'pbot'
            self.base_user = "pythonbot"
            self.level = "WARNING"
            try:
                self.chat = int(input("Input tg chat for logging (add your bot as admin in this chat):"))
            except TypeError:
                self.chat = 0
            if passw == "":
                passw = input("Введите пароль для бд: ")
            self.base_password = passw
        self.list_of_configs = ['token', 'WEBHOOK_HOST', 'WEBHOOK_PORT', 'WEBHOOK_LISTEN', 'WEBHOOK_SSL_CERT',
                                'WEBHOOK_SSL_PRIV',
                                'WEBHOOK_URL_BASE', 'WEBHOOK_URL_PATH', 'base_host', 'base_port', 'base_dbname',
                                'base_user', 'base_password', 'level', 'chat']

    def create_yaml(self):
        self.config = {
            'token': self.token,
            'WEBHOOK_HOST': self.WEBHOOK_HOST,
            'WEBHOOK_PORT': self.WEBHOOK_PORT,
            'WEBHOOK_LISTEN': self.WEBHOOK_LISTEN,
            'WEBHOOK_SSL_CERT': self.WEBHOOK_SSL_CERT,
            'WEBHOOK_SSL_PRIV': self.WEBHOOK_SSL_PRIV,
            'WEBHOOK_URL_BASE': self.WEBHOOK_URL_BASE,
            'WEBHOOK_URL_PATH': self.WEBHOOK_URL_PATH,
            'base_host': self.base_host,
            'base_port': self.base_port,
            'base_dbname': self.base_dbname,
            'base_user': self.base_user,
            'base_password': self.base_password
        }
        self.logging_config = {
            'version': 1,
            'disable_existing_loggers': False,

            'formatters': {
                'default_formatter': {
                    'format': '[%(levelname)s:%(asctime)s] %(module)s : %(funcName)s : %(message)s'
                },
            },

            'handlers': {
                'stream_handler': {
                    'class': 'logging.StreamHandler',
                    'formatter': 'default_formatter',
                },
                'file_handler': {
                    'class': 'logging.FileHandler',
                    'formatter': 'default_formatter',
                    'filename': 'frontend.log'
                },
                'telegram_handler': {
                    'class': 'telegramBot.telebot_logger.TelegramBotHandler',
                    'chat_id': self.chat,
                    'token': self.token,
                    'formatter': 'default_formatter',
                },
            },

            'loggers': {
                'main_logger': {
                    'handlers': ['stream_handler', 'file_handler', 'telegram_handler'],
                    'level': self.level,
                    'propagate': False
                }
            }
        }
        self.config['logging_config'] = self.logging_config
        with open('settings.yaml', 'w') as f:
            yaml.safe_dump(self.config, f)
        logging.config.dictConfig(self.logging_config)
        self.logger = logging.getLogger('main_logger')

    def change(self, par_dict: dict):  # parameter : attribute
        try:
            for parameter, attribute in par_dict.items():
                if parameter in self.list_of_configs:
                    if type(attribute) == str:
                        exec('self.' + parameter + "='" + attribute + "'")
                    else:
                        exec('self.' + parameter + '=' + attribute)
                    self.logger.info("Changed %s in settings", (parameter,))
                else:
                    self.logger.warning("Someone tried to change setting %s that does not exists", (parameter,))
            self.WEBHOOK_URL_BASE = "https://%s:%s", (self.WEBHOOK_HOST, self.WEBHOOK_PORT)
            self.WEBHOOK_URL_PATH = "/%s/", (self.token,)
            self.create_yaml()
            self.logger.info("yaml was recreated")
        except ValueError:
            self.logger.exception("Exception occurred during changing settings")

    # gets postgres port from running processes. should launch from root user
    def get_db_port(self):
        YamlMaster.change_config_file()
        proc = Popen('netstat -plunt', shell=True, stdout=PIPE).communicate()[0]
        proc = str(proc).split('\n')
        postgres_proc = [proc2 for proc2 in proc if 'postgres' or '-' in proc2 and 'tcp6' not in proc2]
        if postgres_proc:
            port = search('[0-9]*\\.[0-9]*\\.[0-9]*\\.[0-9]*\\:[0-9]*', postgres_proc[0]).group(0).split(':')[1]
            return port
        else:
            checking = Popen('systemctl restart postgresql',
                             shell=True,
                             stdout=PIPE)
            checking.wait()
            self.get_db_port()

    @staticmethod
    def change_config_file():
        start_as_systemctl = Popen('systemctl start postgresql',
                                   shell=True,
                                   stdout=PIPE)
        start_as_systemctl.wait()
        start_as_service = Popen('service postgresql start',
                                 shell=True,
                                 stdout=PIPE)
        start_as_service.wait()
        proc = subprocess.Popen("ps aux  | grep '/etc/postgresql/[0-9.]*/main/postgresql.conf'",
                                stdout=PIPE,
                                shell=True).communicate()[0]
        reg = re_compile(r'/etc/postgresql/[0-9.]*/main/postgresql.conf')
        config_file = search(reg, str(proc)).group()
        path_to_config = config_file.rsplit('/', 1)[0] + '/'

        create_backup = subprocess.Popen(
            'sudo sh -c  "sudo cp {config}pg_hba.conf {config}pg_hba.conf.bak"'.format(config=path_to_config),
            shell=True,
            stdout=PIPE)
        create_backup.wait()

        add_line = subprocess.Popen(
            'sudo sh -c  "sudo sh -c "echo local all pythonbot md5 >> {config}pg_hba.conf"'.format(
                config=path_to_config),
            shell=True,
            stdout=PIPE)
        add_line.wait()

        push_pythonbot = subprocess.Popen(
            'sudo sh -c  "sudo sh -c "echo listen_address = {address} >> {config}"'.format(config=config_file,
                                                                                           address='"localhost"'),
            shell=True,
            stdout=PIPE)
        push_pythonbot.wait()


if __name__ == '__main__':
    os.remove(os.getcwd()+"/settings.yaml")
    yml = YamlMaster()
    yml.create_yaml()
